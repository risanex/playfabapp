//
//  PlayFabAppTests.swift
//  PlayFabAppTests
//
//  Created by MohammadAkbari on 16/02/2021.
//
import RxSwift
import XCTest
@testable import PlayFabApp

class PlayFabAppTests: XCTestCase {
    
    
    let loginService = LoginService()
    let leaderService = LeaderBoardService()
    private let disposeBag = DisposeBag()
    
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    
    func testExampleLoginApi() throws {
        
        let username = "userTesti"
        let password = "Salam@99@Test"
        
        self.asynchronous {
            APIClient.authentication.userLogin(username: username, password: password) { model in
                if model.code == 200 {
                    XCTAssertNotNil(model.data,"Data not nil")
                } else {
                    XCTAssertNil(model.data,"Data nil")
                }
            }
        }
        
    }
    
    func testExampleLeaderApi() throws {
        self.asynchronous {
            APIClient.home.leaderBoard { model in
                if model.code == 200 {
                    XCTAssertNotNil(model.data,"Data not nil")
                } else {
                    XCTAssertNil(model.data,"Data nil")
                }
            }
        }
    }
    
    
    func testExmapleLoginService() throws {
        self.asynchronous {
            self.leaderService.leaderBoard().subscribe { model in
                if model.element != nil {
                    XCTAssertNotNil(model.element,"Data not nil")
                } else {
                    XCTAssertNil(model.element,"Data nil")
                }
            }.disposed(by: self.disposeBag)
        }
    }
    
    func testExmapleLeaderService() throws {
        let username = "userTesti"
        let password = "Salam@99@Test"
        self.asynchronous {
            self.loginService.userLogin(username: username, password: password).subscribe { model in
                if model.element != nil {
                    XCTAssertNotNil(model.element,"Data not nil")
                } else {
                    XCTAssertNil(model.element,"Data nil")
                }
            }.disposed(by: self.disposeBag)
        }
    }
   
    
    func asynchronous(queue: DispatchQueue = DispatchQueue(label: "some queue"),callBack:@escaping()->Void) {
        queue.async {
            callBack()
        }
    }
}
