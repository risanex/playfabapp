//
//  APIClientSettings.swift
//  TrafficMap
//
//  Created by MohammadAkbari on 12/02/2021.
//

import Foundation
import Alamofire
import UIKit
import SVProgressHUD

// MARK: - fetchSettings : use the Appdelegate
class FetchSettings {
    
    static let defults = FetchSettings()
    public var fetchTimeoutIntervalForRequest:Double = 15
    public var error_Show:Bool = true
}


// MARK: - service complations
public typealias Service<Response> = (_ completionHandler: @escaping (Response) -> Void) -> Void

// MARK: - is network or not
public var reachability = NetworkReachabilityManager()

// MARK: - settings for reguest
class FetchSession: Session {
    static let session: FetchSession = {
        let configuration = URLSessionConfiguration.default
        configuration.headers = .default
        configuration.timeoutIntervalForRequest = FetchSettings.defults.fetchTimeoutIntervalForRequest // as seconds, you can set your request timeout
        configuration.timeoutIntervalForResource = FetchSettings.defults.fetchTimeoutIntervalForRequest // as seconds, you can set your resource timeout
        configuration.requestCachePolicy = .useProtocolCachePolicy
        return FetchSession(configuration: configuration)
    }()
}

// MARK: - enumt params
public enum RequestParameters {
    
    case body([String:Any]?)
    case url([String: Any]?)
    case form([String: Any]?)
    
    var parameters: [String: Any]? {
        switch self {
        case .body(let parameters), .url(let parameters),.form(let parameters):
            return parameters
        }
    }
}
