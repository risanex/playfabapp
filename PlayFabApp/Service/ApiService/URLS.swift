//
//  URLS.swift
//  TrafficMap
//
//  Created by MohammadAkbari on 12/02/2021.
//

import Foundation


class URLS {
    
    
    static  let shared = URLS()
    let titleID = "3766a"
    
    private init () {
        //singliton
    }
    
    lazy var api = "https://\(titleID).playfabapi.com/Client/"
    
    
    
}
