//
//  Fetch.swift
//  TrafficMap
//
//  Created by MohammadAkbari on 12/02/2021.
//

import Alamofire
import UIKit
import SVProgressHUD

// MARK: - use propery wrapper make for easy use

@propertyWrapper
public class Fetch<T> where T: Codable {
    
    // MARK: - varibales
    
    private var url: String?
    private var path: URLConvertible?
    private var method: HTTPMethod = .get
    private var headers: HTTPHeaders = .default
    private var parameters: [String:Any]? = nil
    private var encoding:ParameterEncoding = URLEncoding.default
    
    // MARK: - return value
    public var projectedValue: Fetch<T> { return self }
    
    // MARK: - init method and url
    public init(_ url:String,method:HTTPMethod? = .get) {
        self.url = url
        self.method = method!
    }
    
    
    // MARK: - set path if url not base
    @discardableResult
    public func set(path:String) -> Self {
        self.path = path
        return self
    }
    
    // MARK: - header for auth
    @discardableResult
    public func set(headers:HTTPHeaders) -> Self {
        self.headers = headers
        return self
    }
    
    // MARK: - parameters , query(url) , formData , body
    @discardableResult
    public func set(parameters:RequestParameters) -> Self {
        switch parameters {
        case .body(let value):
            self.parameters = value
            self.encoding = JSONEncoding.default
        case .url(let value):
            self.parameters = value
            self.encoding = URLEncoding.queryString
        case .form(let value):
            self.parameters = value
            self.encoding = URLEncoding.default
        }
        return self
    }
    
    
    // MARK: - value set
    public var wrappedValue: Service<T> {
        get {
            return { completion in
                self.skipNetwork { result in
                    completion(result)
                }
            }
        }
    }
    
    // MARK: - check network
    fileprivate func skipNetwork(completion:@escaping(T)->Void) {
        if reachability?.isReachable ?? true{
            
            self.fetchReguest { result in
                completion(result)
            }
        } else {
            
            let action =  UIAlertAction(title: "Try", style: .default, handler: { _ in
                self.skipNetwork { (result) in
                    completion(result)
                }
            })
            
            let cancel =  UIAlertAction(title: "Cancel", style: .destructive, handler: { _ in})
            Alert.share.setAlertError(title: "Network Error", message: "Please Check Conections", action: [action,cancel])
        }
    }
    
    
    
    
    
    // MARK: - regsuet use AF from session
    fileprivate func fetchReguest(completion:@escaping(T)->Void) {
        DispatchQueue.global().async {
            DispatchQueue.main.async {
                SVProgressHUD.show()
            }
        }
        
        FetchSession.session.request("\(self.url ?? "")\(self.path ?? "")", method: self.method,parameters: self.parameters, encoding:self.encoding , headers: self.headers, interceptor: nil).responseData(queue:.main) { (response: AFDataResponse<Data>) in
            DispatchQueue.global().async {
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
            }
            
            guard let statusCode = response.response?.statusCode else {return}
            
            
            print("statusCode: \(statusCode)")
            
            switch response.result {
            case .success(let data):
                
                do {
                    
                    if statusCode == 403 || statusCode == 401  {
                    } else  {
                        
                    }
                    
                    let responseData = try JSONDecoder().decode(T.self, from: data)
                    
                    completion(responseData)

                } catch let error {
                    print("catch_error: \(error.localizedDescription)")
                }
                
            case .failure(let error):
                let action =  UIAlertAction(title: "Try", style: .default, handler: { _ in
                                            self.skipNetwork { (result) in
                        completion(result)
                    }
                })
                 let cancel =  UIAlertAction(title: "Cancel", style: .destructive, handler: { _ in})
                Alert.share.setAlertError(title: "Error", message: error.localizedDescription, action: [action,cancel])
                print("AF_Exit_Error: \(error)")
            }

            
            
            
            
        }.responseJSON(queue: .main) { response in
            // MARK: - show alert
            if response.error?.errorDescription != nil {
                let action =  UIAlertAction(title: "Try", style: .default, handler: { _ in
                    self.skipNetwork { (result) in
                        completion(result)
                    }
                })
                 let cancel =  UIAlertAction(title: "Cancel", style: .destructive, handler: { _ in})
                
                if response.error?.errorDescription == "URLSessionTask failed with error: A server with the specified hostname could not be found." {
                    Alert.share.setAlertError(title: "Error", message: "Please Checked Network.", action: [action,cancel])
                } else if response.error?.errorDescription == "URLSessionTask failed with error: The request timed out." {
                    Alert.share.setAlertError(title: "Error", message: "Please Checked Network.", action: [action,cancel])
                } else {
                    Alert.share.setAlertError(title: "Error", message: response.error?.errorDescription ?? "", action: [action,cancel])

                }
                print("Fetch Error:\(response.error?.errorDescription ?? "")")
            }
            
        }
    }
}
