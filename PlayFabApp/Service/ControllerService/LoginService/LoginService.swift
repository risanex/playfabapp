//
//  ApiLoginService.swift
//  PlayFabApp
//
//  Created by MohammadAkbari on 16/02/2021.
//

import Foundation
import UIKit
import RxSwift

public class LoginService:LoginApiDelegate,DispatchQueueDelegate {
    
    
    var dispatchQueue: DispatchQueue = .global()
    
    
    
    func userLogin(username: String, password: String)->Observable<AuthenticationModel> {
        return Observable.create { observer -> Disposable in
            
            self.dispatchQueue.async {
                APIClient.authentication.userLogin(username: username, password: password) { model in
                    if model.code == 200 {
                        observer.onNext(model)
                    } else {
                        observer.onError(NSError(domain:"", code: model.code ?? 0, userInfo: [:]))
                    }
                }
            }
            return Disposables.create {}
        }
    }

    
    func gotToMain(_ viewController:UIViewController) {
        let vc = HomeController()
        vc.navigationItem.title = "Home"
        viewController.navigationController?.pushViewController(vc, animated: true)
    }
    
}
