//
//  LeaderBoardService.swift
//  PlayFabApp
//
//  Created by MohammadAkbari on 16/02/2021.
//

import Foundation
import RxSwift
import RxCocoa

public class LeaderBoardService:leaderBoardDelegate,DispatchQueueDelegate {
    
    
    var dispatchQueue: DispatchQueue = .global()
    var leaderBoardElement:BehaviorRelay<[Leaderboard]>?
    
    func leaderBoard() -> Observable<[Leaderboard]> {
        return Observable.create { observer -> Disposable in
            self.dispatchQueue.async {
                APIClient.home.leaderBoard { model in
                    if model.code == 200 {
                        self.leaderBoardElement = .init(value: model.data?.leaderboard ?? [])
                        observer.onNext(model.data?.leaderboard ?? [])
                    } else {
                        observer.onError(NSError(domain:"", code: model.code ?? 0, userInfo: [:]))
                    }
                }
            }
            
            return Disposables.create {}
        }
    }
    
    
    
    
}
