//
//  APIClientExtentions.swift
//  PlayFabApp
//
//  Created by MohammadAkbari on 16/02/2021.
//

import Foundation


struct Model:Codable {
    
}
extension APIClient {
    
   // MARK: -  login
    @Fetch<AuthenticationModel>(URLS.shared.api,method:.post)
    static var userLogin:Service<AuthenticationModel>
    
    // MARK: - List LeaderBoard
     @Fetch<LeaderBoardModel>(URLS.shared.api,method:.post)
     static var leaderBoard:Service<LeaderBoardModel>
     
}
