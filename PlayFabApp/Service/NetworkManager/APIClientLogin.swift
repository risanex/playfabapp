//
//  APIClientExtentions.swift
//  TrafficMap
//
//  Created by MohammadAkbari on 12/02/2021.
//

import Foundation




extension APIClient {
    
    class authentication {
        
        class func userLogin(username:String,password:String,complation:@escaping (AuthenticationModel)->Void) {
            
            APIClient.$userLogin
                .set(path:"LoginWithPlayFab")
                .set(parameters: .body([
                    "Password":password,
                    "Username":username,
                    "TitleId":URLS.shared.titleID,
                ]))
            
            APIClient.userLogin { model in
                complation(model)
            }
        }
    }
}
