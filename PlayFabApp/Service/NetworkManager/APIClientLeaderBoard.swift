//
//  APIClientLeaderBoard.swift
//  PlayFabApp
//
//  Created by MohammadAkbari on 16/02/2021.
//

import Foundation



extension APIClient {
    
    class home {
        
        class func leaderBoard(complation:@escaping (LeaderBoardModel)->Void) {
            
            APIClient.$leaderBoard
                .set(path:"GetLeaderboard")
                .set(headers: ["X-Authorization":AppService.shared.token])
                .set(parameters: .body([
                    "MaxResultsCount":10,
                    "StartPosition":"0",
                    "StatisticName":"leaderBoard"
                ]))

            
            APIClient.leaderBoard { model in
                complation(model)
            }
        }
    }
}
