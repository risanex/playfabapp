//
//  AppService.swift
//  PlayFabApp
//
//  Created by MohammadAkbari on 16/02/2021.
//

import UIKit


class AppService {
     
    static let shared = AppService()
    private let userDefults = UserDefaults.standard
    private init() {}
    
    
    var token:String {
        get {
            return userDefults.string(forKey: "token") ?? .init()
        }
        
        set {
            userDefults.setValue(newValue, forKey: "token")
            userDefults.synchronize()
        }
    }
    
}
