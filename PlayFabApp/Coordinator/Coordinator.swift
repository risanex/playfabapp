//
//  Coordinator.swift
//  PlayFabApp
//
//  Created by MohammadAkbari on 16/02/2021.
//

import UIKit


class Coordinator {
    
    
    private var window:UIWindow
    private var navigaion:UINavigationController?
    
    // inint
    init(window:UIWindow) {
        self.window = window
    }
    
    // setup navigations
    func addNavigation() {
        self.navigaion = UINavigationController()
    }
    
    // begin controller
    func start() {
        let vc = LoginController.instanstion()
        self.navigaion = .init(rootViewController: vc)
        self.window.rootViewController = self.navigaion
        self.window.makeKeyAndVisible()
    }
}

