//
//  LeaderBoardModel.swift
//  PlayFabApp
//
//  Created by MohammadAkbari on 16/02/2021.
//

import Foundation

// MARK: - LeaderBoardModel
struct LeaderBoardModel: Codable {
    let code: Int?
    let status: String?
    let data: DataClass?

    enum CodingKeys: String, CodingKey {
        case code = "code"
        case status = "status"
        case data = "data"
    }
}

// MARK: - DataClass
struct DataClass: Codable {
    let leaderboard: [Leaderboard]?
    let version: Int?

    enum CodingKeys: String, CodingKey {
        case leaderboard = "Leaderboard"
        case version = "Version"
    }
}

// MARK: - Leaderboard
struct Leaderboard: Codable {
    let playFabId: String?
    let displayName: String?
    let statValue: Int?
    let position: Int?
    let profile: Profile?

    enum CodingKeys: String, CodingKey {
        case playFabId = "PlayFabId"
        case displayName = "DisplayName"
        case statValue = "StatValue"
        case position = "Position"
        case profile = "Profile"
    }
}

// MARK: - Profile
struct Profile: Codable {
    let publisherId: PublisherId?
    let titleId: TitleId?
    let playerId: String?
    let displayName: String?

    enum CodingKeys: String, CodingKey {
        case publisherId = "PublisherId"
        case titleId = "TitleId"
        case playerId = "PlayerId"
        case displayName = "DisplayName"
    }
}

enum PublisherId: String, Codable {
    case f2456482Bb8A571F = "F2456482BB8A571F"
}

enum TitleId: String, Codable {
    case the3766A = "3766A"
}
