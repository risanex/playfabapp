//
//  AuthenticationModel.swift
//  PlayFabApp
//
//  Created by MohammadAkbari on 16/02/2021.
//

import Foundation

// MARK: - AuthenticationModel
struct AuthenticationModel: Codable {
    let code: Int?
    let status: String?
    let data: DataClassModel?

    enum CodingKeys: String, CodingKey {
        case code = "code"
        case status = "status"
        case data = "data"
    }
}

// MARK: - DataClass
struct DataClassModel: Codable {
    let sessionTicket: String?
    let playFabId: String?
    let newlyCreated: Bool?
    let settingsForUser: SettingsForUser?
    let lastLoginTime: String?
    let entityToken: EntityToken?
    let treatmentAssignment: TreatmentAssignment?

    enum CodingKeys: String, CodingKey {
        case sessionTicket = "SessionTicket"
        case playFabId = "PlayFabId"
        case newlyCreated = "NewlyCreated"
        case settingsForUser = "SettingsForUser"
        case lastLoginTime = "LastLoginTime"
        case entityToken = "EntityToken"
        case treatmentAssignment = "TreatmentAssignment"
    }
}

// MARK: - EntityToken
struct EntityToken: Codable {
    let entityToken: String?
    let tokenExpiration: String?
    let entity: Entity?

    enum CodingKeys: String, CodingKey {
        case entityToken = "EntityToken"
        case tokenExpiration = "TokenExpiration"
        case entity = "Entity"
    }
}

// MARK: - Entity
struct Entity: Codable {
    let id: String?
    let type: String?
    let typeString: String?

    enum CodingKeys: String, CodingKey {
        case id = "Id"
        case type = "Type"
        case typeString = "TypeString"
    }
}

// MARK: - SettingsForUser
struct SettingsForUser: Codable {
    let needsAttribution: Bool?
    let gatherDeviceInfo: Bool?
    let gatherFocusInfo: Bool?

    enum CodingKeys: String, CodingKey {
        case needsAttribution = "NeedsAttribution"
        case gatherDeviceInfo = "GatherDeviceInfo"
        case gatherFocusInfo = "GatherFocusInfo"
    }
}

// MARK: - TreatmentAssignment
struct TreatmentAssignment: Codable {
    let variants: [String]?
    let variables: [String]?

    enum CodingKeys: String, CodingKey {
        case variants = "Variants"
        case variables = "Variables"
    }
}
