//
//  HomeController.swift
//  PlayFabApp
//
//  Created by MohammadAkbari on 16/02/2021.
//

import UIKit


class HomeController:ViewController {
    
    lazy var tableView:UITableView = {
        let tv = UITableView()
        tv.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tv.separatorStyle = .none
        return tv
    }()
    
    
    override func loadView() {
        self.setupLayout()
    }
    
    // service
    private let leaderService = LeaderBoardService()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupLayout()
        self.setupView()
        self.rxBind()
    }
}



//MARK: set Data bind
extension HomeController:RXMethodDelegate {
    
    func rxBind() {
        // auot load from rx observable
        self.leaderService.leaderBoard().asObservable().bind(to: tableView.rx.items(cellIdentifier: "cell")) { row , item, cell in
            cell.textLabel?.text = "\(item.displayName?.uppercased() ?? "") = \(item.playFabId ?? "")"
        }.disposed(by: self.disposeBag)
        
        tableView.rx.itemSelected.subscribe(onNext:{[weak self] indexPath in
            guard let self = self else {return}
            self.tableView.deselectRow(at: indexPath, animated: true)
            let item = self.leaderService.leaderBoardElement?.value[indexPath.row]
            Alert.share.alert(title:item?.displayName?.uppercased() ?? "", message: item?.playFabId ?? "")
        }).disposed(by: self.disposeBag)

    }

}


// MARK: setView
extension HomeController:SetViewDelegate {
    
    
    func setupView() {
        self.view.backgroundColor = .white
    }
    
    func setupLayout() {
        view = tableView
    }
    
    
    
}


