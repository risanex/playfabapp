//
//  LoginController.swift
//  PlayFabApp
//
//  Created by MohammadAkbari on 16/02/2021.
//

import UIKit
import SnapKit
import RxSwift
import RxCocoa

class LoginController: ViewController {
    
    
    // for Coordinator show
    static func instanstion()->LoginController {
        let vc = LoginController()
        return vc
    }
    
    // ui etc ...
    
    private let btnLogin:UIButton = {
        let btn = UIButton()
        btn.layer.cornerRadius = 5
        btn.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
        btn.setTitle("Login", for: .normal)
        btn.setTitleColor(.white, for: .normal)
        btn.titleLabel?.font = .boldSystemFont(ofSize: 18)
        return btn
    }()

    
    private let loginService = LoginService()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
        self.setupLayout()
        self.rxBind()
    }


}



//MARK: set Data bind
extension LoginController:RXMethodDelegate {
    
    func rxBind() {
        btnLogin.rx.tap.subscribe {[weak self] _ in
            guard let self = self else {return}
            self.loginService.userLogin(username: "userTesti", password: "Salam@99@Test").subscribe {[weak self] model in
                guard let self = self else {return}
                if let sessionTicket = model.element?.data?.sessionTicket {
                    AppService.shared.token = sessionTicket
                }
                self.loginService.gotToMain(self)
            }.disposed(by: self.disposeBag)

        }.disposed(by: self.disposeBag)
    }

}


// MARK: setView
extension LoginController:SetViewDelegate {
    
    
    
    func setupView() {
        self.view.backgroundColor = .white
        self.navigationItem.title = "Login"
    }
    
    
    func setupLayout() {
        self.view.addSubview(btnLogin)
        self.btnLogin.snp.makeConstraints { make in
            make.center.equalTo(view)
            make.width.equalTo(view.frame.width - 20)
            make.height.equalTo(50)
        }
        
    }
    
    
    
}

