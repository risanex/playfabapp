//
//  Alert.swift
//  PlayFabApp
//
//  Created by MohammadAkbari on 16/02/2021.
//

import UIKit


// MARK: - alert for easy use

class Alert {
    
    class share {
        
        class func setAlertError(title:String,message:String,action:[UIAlertAction]) {
            DispatchQueue.main.async {
                let alert = UIAlertController(title:title, message: message == "" ? nil : message, preferredStyle: .alert)
                
                action.forEach { actionAlert in
                    alert.addAction(actionAlert)
                }
                if let keyWindow = UIWindow.key {
                    if FetchSettings.defults.error_Show {
                        keyWindow.rootViewController?.present(alert, animated: true, completion: nil)
                    }
                }
            }
        }
        class func alert(title:String,message:String,action:[UIAlertAction]? = nil) {
            DispatchQueue.main.async {
                let alert = UIAlertController(title:title, message: message == "" ? nil : message, preferredStyle: .alert)
                if action?.isEmpty ?? true {
                    let actionOk = UIAlertAction(title: "OK", style: .destructive) { _ in
                        return
                    }
                    alert.addAction(actionOk)
                } else {
                    action?.forEach { actionAlert in
                        alert.addAction(actionAlert)
                    }
                    
                }
                
                if let keyWindow = UIWindow.key {
                    keyWindow.rootViewController?.present(alert, animated: true, completion: nil)
                }
                
            }
            
            
        }
        
        class func sheet(title:String,message:String,action:[UIAlertAction]? = nil) {
            DispatchQueue.main.async {
                let alert = UIAlertController(title:title, message: message == "" ? nil : message, preferredStyle: .actionSheet)
                
                action?.forEach { actionAlert in
                    alert.addAction(actionAlert)
                }
                if let keyWindow = UIWindow.key {
                    keyWindow.rootViewController?.present(alert, animated: true, completion: nil)
                }
            }
            
            
        }
        
    }
}
