//
//  SetViewDelegate.swift
//  PlayFabApp
//
//  Created by MohammadAkbari on 16/02/2021.
//

import Foundation
import RxSwift
import UIKit

//MARK: add protocol for set view dont repat other programing

protocol SetViewDelegate {
    // add  view for controller
    func setupView()
    // add constraint view
    func setupLayout()
}


protocol RXMethodDelegate {
    // bind data rx
    func rxBind()
}


protocol LoginApiDelegate {
    //api user login protocol from observbles 
    func userLogin(username:String,password:String)->Observable<AuthenticationModel>
}


protocol leaderBoardDelegate {
    //api user leader protocol from observbles
    func leaderBoard()->Observable<[Leaderboard]>
}

protocol DispatchQueueDelegate {
    //api user leader protocol from observbles
    var dispatchQueue:DispatchQueue {get set}
}
