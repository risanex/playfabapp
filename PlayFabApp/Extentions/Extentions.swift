//
//  Extentions.swift
//  PlayFabApp
//
//  Created by MohammadAkbari on 16/02/2021.
//

import UIKit


extension UIWindow {
    static var key: UIWindow? {
        return UIApplication.shared.windows.first { $0.isKeyWindow }
    }
}
